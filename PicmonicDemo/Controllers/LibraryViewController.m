//
//  LibraryViewController.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "LibraryViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "STCardDetailViewController.h"
#import "SlideNavigationController.h"
#import "STCardTableViewCell.h"
#import "SVProgressHud.h"
#import "STCardManager.h"
#import "STCard.h"

@interface LibraryViewController () <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** 
 Array of STCards retrieved from server
 */
@property (strong, nonatomic) NSArray *cardArray;

/** 
 Requests users cards through STCardManager
 */
- (void)requestCards;

/**
 Display not yet implemented alert
 */
- (IBAction)NotYetImplementedAlert:(id)sender;

@end

@implementation LibraryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestCards];
}

- (void)requestCards {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    STCardManager *cardManager = [[STCardManager alloc] init];
    [cardManager retrieveUsersCardsWithCompletionHandler:^(NSArray *cards) {
        self.cardArray = cards;
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
    }];
}

- (IBAction)NotYetImplementedAlert:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Coming Soon!"
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"Ok!"
                      otherButtonTitles:nil, nil] show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SlideNavigationControllerDelegate delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

#pragma mark - UITableViewCell delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cardArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    STCardTableViewCell *cell = (STCardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CardCell"];
    
    STCard *card = (self.cardArray)[indexPath.row];
    cell.cardTitleLabel.text = card.title;
    
    [cell.cardImageView sd_setImageWithURL:[NSURL URLWithString:card.imageUrl]
                      placeholderImage:nil];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    STCardDetailViewController *detailVC = segue.destinationViewController;
    
    STCard *card = [self.cardArray objectAtIndex:indexPath.row];
    detailVC.card = card;
}

@end
