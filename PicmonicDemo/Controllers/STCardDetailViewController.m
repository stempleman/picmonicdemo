//
//  STCardDetailViewController.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/6/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "STCardDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "STCardDetailTableViewCell.h"
#import "STCardSection.h"
#import "STCard.h"

@interface STCardDetailViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 Card Sections from self.card
 */
@property (strong, nonatomic) NSArray *cardSectionsArray;

/**
 Pullable view that allows for expansion of tableview/imageview height
 */
@property (weak, nonatomic) IBOutlet UIView *pullableView;

/**
 Pullable view height constraint, adjust y position while panning
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pullableViewVerticalSpaceHeightConstraint;

/**
 NSIndexPath of expanded cell
 */
@property (strong, nonatomic) NSIndexPath *expandedIndexPath;

/**
 Display large image
 */
@property (weak, nonatomic) IBOutlet UIImageView *largeImageView;

/**
 Pans y position of pullableViews contstraint when pan is recognized
 
 @param sender The UIPanGestureRecognizer which detects panning
 */
- (void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)sender;

/**
 Configures PanGestureRecogonizer for self.pullableView
 */
- (void)addPanGestureRecToPullableView;

/**
 UIImage for selected/deselected state of STCardSection selected property
 
 @param section The STCardSection to be at indexPath
 @param indexPath The NSIndexPath of section
 
 @return UIImage for state
 */
- (UIImage *)imageForSection:(STCardSection *)section indexPath:(NSIndexPath *)indexPath;

/**
 Download/cache image from server
 */
- (void)requestImage;

/**
 Pop to presenting view controller
 */
- (IBAction)notYetImplementedAlert:(id)sender;

/**
 Display not yet implemented alert
 */
- (IBAction)popToLibraryViewController:(id)sender;

@end

@implementation STCardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cardSectionsArray = self.card.cardSectionsArray;
    
    [self addPanGestureRecToPullableView];
    [self requestImage];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.title = self.card.title;
}

- (void)requestImage {
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:self.card.largeImageUrl]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [self.largeImageView setImage:image];
                            }
                        }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)popToLibraryViewController:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)notYetImplementedAlert:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Coming Soon!"
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"Ok!"
                      otherButtonTitles:nil, nil] show];
}

#pragma mark - PullableView gesture recognizer

- (void)addPanGestureRecToPullableView {
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    [self.pullableView addGestureRecognizer:panGestureRecognizer];
}

#define TOP_MARGIN 100
#define BOTTOM_MARGIN 230

- (void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)sender {
    if(sender.state == UIGestureRecognizerStateBegan){
    } else if(sender.state == UIGestureRecognizerStateChanged){
        CGPoint translation = [sender translationInView:self.view];
        
        //Update the constraint's constant
        self.pullableViewVerticalSpaceHeightConstraint.constant += translation.y;
        
        // Assign the frame's position only for checking it's fully on the screen
        CGRect recognizerFrame = sender.view.frame;
        recognizerFrame.origin.y = self.pullableViewVerticalSpaceHeightConstraint.constant;
    
        //scrollable area, +- SCROLLABLE_MARGIN view.origin and view.height
        CGRect scrollableFrame = CGRectMake(0, TOP_MARGIN, self.view.bounds.size.width, self.view.bounds.size.height - BOTTOM_MARGIN - TOP_MARGIN);
        
        // Check if recognizerFrame is completely inside scrollable area
        if(!CGRectContainsRect(scrollableFrame, recognizerFrame)) {
            if (self.pullableViewVerticalSpaceHeightConstraint.constant < CGRectGetMinY(scrollableFrame)) {
                self.pullableViewVerticalSpaceHeightConstraint.constant = TOP_MARGIN;
            } else if (self.pullableViewVerticalSpaceHeightConstraint.constant + CGRectGetHeight(recognizerFrame) > CGRectGetHeight(scrollableFrame)) {
                self.pullableViewVerticalSpaceHeightConstraint.constant = scrollableFrame.origin.y + scrollableFrame.size.height - CGRectGetHeight(recognizerFrame);
            }
        }
        
        //Layout the View
        [self.view layoutIfNeeded];
    } else if(sender.state == UIGestureRecognizerStateEnded){
    }
    
    [sender setTranslation:CGPointMake(0, 0) inView:self.view];
}

#pragma mark - UITableViewCell delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cardSectionsArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    STCardDetailTableViewCell *cell = (STCardDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CardDetailCell"];
    
    STCardSection *section = (self.cardSectionsArray)[indexPath.row];
    [cell.cardSectionImageView setImage:[self imageForSection:section
                                                    indexPath:indexPath]];
    
    cell.cardParagraphTitleLabel.text = section.title;
    cell.cardParagraphSubtitleLabel.text = section.subTitle;
    cell.cardParagraphDescriptionLabel.text = section.cardDescription;

    // set line number label
    if(indexPath.row == 0)
        cell.sectionNumberLabel.text = @"";
     else
        cell.sectionNumberLabel.text = [NSString stringWithFormat:@"%ld",
                                        (long)indexPath.row];
    
    // hide/unhide description label depending on if cell is expanded
    cell.cardParagraphDescriptionLabel.hidden = [indexPath compare:self.expandedIndexPath] == NSOrderedSame ? NO : YES;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Expanded height
    if ([indexPath compare:self.expandedIndexPath] == NSOrderedSame)
        return 100.0;
    
    return 65.0; // Normal height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //toggle selected
//    [self.cardSectionsArray enumerateObjectsUsingBlock:^(STCardSection *obj, NSUInteger idx, BOOL *stop) {
//        if(idx != indexPath.row)
//            obj.selected = NO;
//        else
//            obj.selected = !obj.selected;
//    }];
    
    [self.card setSelectedSection:[self.cardSectionsArray objectAtIndex:indexPath.row]];
    
    // dynamic height
    [tableView beginUpdates];
    if ([indexPath compare:self.expandedIndexPath] == NSOrderedSame) {
        self.expandedIndexPath = nil;
    } else {
        self.expandedIndexPath = indexPath;
    }
    [self.tableView reloadData];
    [tableView endUpdates];
    
    //scroll table view to top
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (UIImage *)imageForSection:(STCardSection *)section indexPath:(NSIndexPath *)indexPath {
    NSString *imageName;
    switch (indexPath.row) {
        case 0: {
            if(section.selected)
                imageName = @"star_filled";
            else
                imageName = @"star";
            break;
        }
        default: {
            if(section.selected)
                imageName = @"circle_filled";
            else
                imageName = @"circle";
            break;
        }
    }
    return [UIImage imageNamed:imageName];
}

- (void)dealloc {
    [self.card setSelectedSection:nil];
}

@end
