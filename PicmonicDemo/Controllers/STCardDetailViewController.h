//
//  STCardDetailViewController.h
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/6/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <UIKit/UIKit.h>

@class STCard;

@interface STCardDetailViewController : UIViewController

/**
 STCard passed into detail VC
 */
@property (nonatomic, strong) STCard *card;

@end
