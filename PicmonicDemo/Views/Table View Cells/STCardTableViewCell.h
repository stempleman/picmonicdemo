//
//  STCardTableViewCell.h
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STCardTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cardTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;

@end
