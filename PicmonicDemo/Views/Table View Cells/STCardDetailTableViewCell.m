//
//  STCardDetailTableViewCell.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/6/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "STCardDetailTableViewCell.h"

@implementation STCardDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
