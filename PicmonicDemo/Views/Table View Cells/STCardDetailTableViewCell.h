//
//  STCardDetailTableViewCell.h
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/6/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STCardDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cardParagraphTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardParagraphSubtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardParagraphDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectionNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardSectionImageView;

@end
