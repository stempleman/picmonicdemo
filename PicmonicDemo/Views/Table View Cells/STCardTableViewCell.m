//
//  STCardTableViewCell.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "STCardTableViewCell.h"

@implementation STCardTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
