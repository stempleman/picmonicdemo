//
//  STCardSection.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/6/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "STCardSection.h"

@implementation STCardSection

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle description:(NSString *)description {
    self = [super init];
    if(self){
        _title = [title copy];
        _subTitle = [subtitle copy];
        _cardDescription = [description copy];
        
        self.selected =  NO;
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [self initWithTitle:[dictionary valueForKey:@"title"]
                      subtitle:[dictionary valueForKey:@"subtitle"]
                   description:[dictionary valueForKey:@"description"]];
    
    return self;
}

@end
