//
//  STCardManager.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "STCardManager.h"
#import "HTTPClient.h"
#import "STCard.h"

@interface STCardManager()

@property (nonatomic, strong) NSArray *retrievedCards;

@end

@implementation STCardManager

- (void)retrieveUsersCardsWithCompletionHandler:(STCardManagerCompletionHandler)completionHandler {
    if(completionHandler && self.retrievedCards.count > 0) //return cards if already retrieved
        completionHandler(self.retrievedCards);

    NSMutableArray *cardArray = [NSMutableArray array];

    [[HTTPClient sharedClient] GET:@"cards.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        for (NSDictionary *cardDictionary in responseObject) {
            [cardArray addObject:[[STCard alloc] initWithDictionary:cardDictionary]];
        }
        
        self.retrievedCards = [cardArray copy];
        
        if(completionHandler)
            completionHandler(self.retrievedCards);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

@end
