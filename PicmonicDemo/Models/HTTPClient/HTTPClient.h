//
//  HTTPClient.h
//  Golfshot-Demo
//
//  Created by Steven  Templeman on 6/19/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface HTTPClient : AFHTTPRequestOperationManager

+ (id)sharedClient;

@end
