//
//  HTTPClient.m
//  Golfshot-Demo
//
//  Created by Steven  Templeman on 6/19/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "HTTPClient.h"

#define BASE_URL @"https://afternoon-gorge-1655.herokuapp.com/"
//#define BASE_URL @"http://0.0.0.0:3000/"

@implementation HTTPClient

+ (id)sharedClient {
    static HTTPClient *__instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:BASE_URL];
        __instance = [[HTTPClient alloc] initWithBaseURL:baseUrl];
    });
    return __instance;
}

@end
