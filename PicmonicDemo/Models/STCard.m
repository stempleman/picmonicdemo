//
//  STCard.m
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import "STCard.h"
#import "STCardSection.h"

@implementation STCard

- (id)initWithTitle:(NSString *)title imageUrl:(NSString *)imageUrl largeImageUrl:(NSString *)largeImageUrl {
    self = [super init];
    if(self) {
        _title = [title copy];
        _imageUrl = [imageUrl copy];
        _largeImageUrl = [largeImageUrl copy];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [self initWithTitle:[dictionary valueForKey:@"title"]
                      imageUrl:[dictionary valueForKey:@"image_url"]
            largeImageUrl:[dictionary valueForKey:@"large_image_url"]];
    
    if(self) { //add card sections
        NSMutableArray *cardSectionsArray = [NSMutableArray array];
        for (NSDictionary *cardSectionDict in [dictionary objectForKey:@"card_sections"]) {
            [cardSectionsArray addObject:[[STCardSection alloc] initWithDictionary:cardSectionDict]];
        }
        self.cardSectionsArray = [cardSectionsArray copy];
    }
    
    return self;
}

- (void)setSelectedSection:(STCardSection *)selectedSection {
    self.selectedSection.selected = NO;
    
    if(selectedSection != self.selectedSection)
        selectedSection.selected = YES;
    
    _selectedSection = selectedSection;
}

@end
