//
//  STCard.h
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <Foundation/Foundation.h>

@class STCardSection;

@interface STCard : NSObject

- (id)initWithTitle:(NSString *)title imageUrl:(NSString *)imageUrl largeImageUrl:(NSString *)largeImageUrl;
- (id)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *imageUrl;
@property (nonatomic, copy, readonly) NSString *largeImageUrl;

@property (nonatomic, strong) STCardSection *selectedSection;

@property (nonatomic, strong) NSArray *cardSectionsArray;


@end
