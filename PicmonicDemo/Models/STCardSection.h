//
//  STCardSection.h
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/6/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STCardSection : NSObject

- (id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle description:(NSString *)description;
- (id)initWithDictionary:(NSDictionary *)dictionary;

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *subTitle;
@property (nonatomic, copy, readonly) NSString *cardDescription;

@property (nonatomic) BOOL selected;

@end
