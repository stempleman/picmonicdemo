//
//  STCardManager.h
//  PicmonicDemo
//
//  Created by Steven  Templeman on 7/3/15.
//  Copyright (c) 2015 Steven Templeman. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^STCardManagerCompletionHandler) (NSArray *cards);

@interface STCardManager : NSObject

- (void)retrieveUsersCardsWithCompletionHandler:(STCardManagerCompletionHandler)completionHandler;

@end
